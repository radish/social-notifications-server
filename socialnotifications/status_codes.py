from enum import IntEnum


class Errors(IntEnum):
    email_already_registered = 0
    user_already_logged_in = 1
    user_does_not_exist = 2
    wrong_password = 3
    user_not_authenticated = 4
    user_already_voted = 5
