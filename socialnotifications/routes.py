from rethinkframework.server.routes import Route
from .endpoints import MainSocket

routes_list = [
    Route('/', MainSocket)
]
