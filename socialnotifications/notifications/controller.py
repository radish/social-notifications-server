from datetime import datetime
from typing import Any, Dict

import msgpack
import pytz
from aiohttp.web_ws import WebSocketResponse
from rethinkframework.db.exceptions import ValidationError
from rethinkframework.db.fields import GeoCircleField

from socialnotifications.authentication.controller import AuthenticationController
from socialnotifications.notifications.models import Notification
from socialnotifications.status_codes import Errors


class NotificationsController:
    def __init__(self, authentication_controller: AuthenticationController, socket: WebSocketResponse):
        self.__authentication_controller = authentication_controller
        self.__socket = socket

    async def __handle_notification_insertion(self, new_notification):
        self.__socket.send_bytes(msgpack.packb({
            'action': 'new_notification',
            'data': new_notification
        }, use_bin_type=True))

    async def __handle_notification_update(self, new_notification):
        self.__socket.send_bytes(msgpack.packb({
            'action': 'updated_notification',
            'data': new_notification
        }, use_bin_type=True))

    def __subscribe(self, username, circle_data):
        circle = GeoCircleField(circle_data)

        subscription_query = Notification.documents.filter(
            Notification.point.intersects(circle.get_serializable())
        ).without('voters_up', 'voters_down', {'point': ['$reql_type$', 'type'], 'time': '$reql_type$'})

        Notification.feed.subscribe(username, self.__handle_notification_insertion, self.__handle_notification_update,
                                    custom_query=subscription_query)

        return circle

    async def add_notification(self, data: Dict[str, Any]):
        user = self.__authentication_controller.user

        if user:
            try:
                data['author'] = user
                notification = Notification(**data)
                notification.rating = 0
                notification.time = datetime.now(pytz.timezone('Europe/Warsaw'))
            except ValidationError as e:
                return {
                    'action': 'add_notification',
                    'result': False,
                    'reason': str(e)
                }

            await notification.save()
            return {
                'action': 'add_notification',
                'result': True,
            }
        else:
            return {
                'action': 'add_notification',
                'result': False,
                'reason': Errors.user_not_authenticated
            }

    async def get_notifications(self, data: Dict[str, Any]):
        user = self.__authentication_controller.user

        if user:
            notifications = await Notification.documents.without(
                'voters_up', 'voters_down',
                {'point': ['$reql_type$', 'type'], 'time': '$reql_type$'}).execute(time_format='raw')

            return {
                'action': 'get_notifications',
                'result': True,
                'data': await notifications.get_serializable()
            }
        else:
            return {
                'action': 'get_notifications',
                'result': False,
                'reason': Errors.user_not_authenticated
            }

    async def subscribe(self, data: Dict[str, Any]):
        user = self.__authentication_controller.user

        if user:
            try:
                self.__subscribe(user.username, data)

                return {
                    'action': 'subscribe',
                    'result': True
                }
            except ValidationError as e:
                return {
                    'action': 'subscribe',
                    'result': False,
                    'reason': str(e)
                }
        else:
            return {
                'action': 'subscribe',
                'result': False,
                'reason': Errors.user_not_authenticated
            }

    async def vote_up(self, data: str):
        user = self.__authentication_controller.user

        if user:
            notification = await Notification.documents.get(data).execute(raw=False)

            if user.email not in notification.voters_up:
                if user.email in notification.voters_down:
                    notification.rating += 1
                    notification.voters_down.remove(user.email)

                notification.rating += 1
                notification.voters_up.append(user.email)
                await notification.save()

                return {
                    'action': 'vote_up',
                    'result': True
                }
            else:
                return {
                    'action': 'vote_up',
                    'result': False,
                    'reason': Errors.user_already_voted
                }
        else:
            return {
                'action': 'vote_up',
                'result': False,
                'reason': Errors.user_not_authenticated
            }

    async def vote_down(self, data: str):
        user = self.__authentication_controller.user

        if user:
            notification = await Notification.documents.get(data).execute(raw=False)

            if user.email not in notification.voters_down:
                if user.email in notification.voters_up:
                    notification.rating -= 1
                    notification.voters_up.remove(user.email)

                notification.rating -= 1
                notification.voters_down.append(user.email)
                await notification.save()

                return {
                    'action': 'vote_down',
                    'result': True
                }
            else:
                return {
                    'action': 'vote_down',
                    'result': False,
                    'reason': Errors.user_already_voted
                }
        else:
            return {
                'action': 'vote_down',
                'result': False,
                'reason': Errors.user_not_authenticated
            }
