from rethinkframework.db.fields import StringField, GeoPointField, ForeignDocumentField, BinaryField, DateTimeField, \
    IntegerField, ListField
from rethinkframework.db.models import DocumentModel

from socialnotifications.authentication.models import User


class Notification(DocumentModel):
    title = StringField()
    description = StringField()
    photo = BinaryField(nullable=True)
    point = GeoPointField()
    rating = IntegerField()
    voters_up = ListField()
    voters_down = ListField()
    time = DateTimeField()
    author = ForeignDocumentField(related_model=User)
