import ujson

import msgpack
from aiohttp import web_reqrep
from rethinkframework.server.routes import WebSocketEndpoint
from typing import Optional

from socialnotifications.actions import ActionsHandler


class MainSocket(WebSocketEndpoint):
    def __init__(self):
        super().__init__()

        self.__actions_handler = None  # type: Optional[ActionsHandler]

    async def on_open(self, request: web_reqrep.Request):
        print('Client connected')

        self.__actions_handler = ActionsHandler(self._ws)

    async def on_text_message(self, message: str):
        print('Message received:', message)
        message = ujson.loads(message)
        response = await self.__actions_handler.handle(message.get('action', 'default'), message.get('data', {}))
        self._ws.send_str(ujson.dumps(response))

    async def on_binary_message(self, message: bytes):
        print('Binary message received:', message)
        message = msgpack.unpackb(message, encoding='utf-8')
        response = await self.__actions_handler.handle(message.get('action', 'default'), message.get('data', {}))
        self._ws.send_bytes(msgpack.packb(response, use_bin_type=True))

    async def on_error(self):
        print('Error')

    async def on_close(self):
        print('Client disconnected')
