SECRET_KEY = 'lol'

APP_NAME = 'socialnotifications'

# Routes main file
ROOT_ROUTES = 'socialnotifications.routes'


# List of modules containing models
MODULES = [
    'socialnotifications.authentication',
    'socialnotifications.notifications'
]

# Database connection data
DATABASE = {
    'host': 'localhost',
    'port': 28015,
    'db': 'social_notifications',
    'auth_key': ''
}
