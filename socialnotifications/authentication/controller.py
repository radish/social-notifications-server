from typing import Any, Dict, Callable

from rethinkframework.db.exceptions import ValidationError
from typing import List

from typing import Optional

from socialnotifications.authentication.models import User
from socialnotifications.status_codes import Errors


class AuthenticationController:
    def __init__(self):
        self.__user = None  # type: Optional[User]
        self.__authentication_callbacks = []  # type: List[Callable[[Any], Any]]

    async def login(self, data: Dict[str, Any]):
        if not self.__user:
            user = await User.documents.get(data['email']).execute(raw=False)
            if not user:
                return {
                    'action': 'login',
                    'result': False,
                    'reason': Errors.user_does_not_exist
                }

            if user.authenticate(data['password']):
                self.__user = user

                for callback in self.__authentication_callbacks:
                    callback(user)

                return {
                    'action': 'login',
                    'result': True,
                }
            else:
                return {
                    'action': 'login',
                    'result': False,
                    'reason': Errors.wrong_password
                }
        else:
            return {
                'action': 'login',
                'result': False,
                'reason': Errors.user_already_logged_in
            }

    async def register(self, data: Dict[str, Any]):
        if await User.documents.get(data['email']).execute():
            return {
                'action': 'register',
                'result': False,
                'reason': Errors.email_already_registered
            }

        if not self.__user:
            try:
                user = User(**data)
                await user.save()

                return {
                        'action': 'register',
                        'result': True,
                }
            except ValidationError as e:
                return {
                    'action': 'register',
                    'result': False,
                    'reason': str(e)
                }
        else:
            return {
                    'action': 'register',
                    'result': False,
                    'reason': Errors.user_already_logged_in
            }

    def add_authentication_callback(self, callback: Callable[[Any], Any]):
        self.__authentication_callbacks.append(callback)

    @property
    def user(self):
        return self.__user
