from rethinkframework.db.fields import StringField, PasswordField, BinaryField, ListField
from rethinkframework.db.models import DocumentModel


class User(DocumentModel):
    primary_key = 'email'

    name = StringField()
    surname = StringField()
    username = StringField()
    email = StringField()
    password = PasswordField()
    avatar = BinaryField(nullable=True)

    def authenticate(self, password: str) -> bool:
        if self.password == password:
            return True
        return False
