import pytest
import msgpack
import websocket

from socialnotifications.status_codes import Errors


def test_subscribing(ws: websocket.WebSocket):
    ws.send_binary(msgpack.packb({
        'action': 'subscribe',
        'data': ((0, 0), 10000)
    }))

    result = msgpack.unpackb(ws.recv(), encoding='utf-8')

    assert result == {
        'action': 'subscribe',
        'result': True
    }


def test_adding_valid_notification(ws: websocket.WebSocket):
    ws.send_binary(msgpack.packb({
        'action': 'add_notification',
        'data': {
            'title': 'Title test',
            'description': 'Description test',
            'point': (0, 0)
        }
    }))

    result = msgpack.unpackb(ws.recv(), encoding='utf-8')

    if result['action'] == 'add_notification':
        assert result == {
            'action': 'add_notification',
            'result': True
        }

        result = msgpack.unpackb(ws.recv(), encoding='utf-8')

        assert result['action'] == 'new_notification'
    elif result['action'] == 'new_notification':
        result = msgpack.unpackb(ws.recv(), encoding='utf-8')

        assert result == {
            'action': 'add_notification',
            'result': True
        }
    else:
        pytest.fail('Invalid action')


def test_getting_notifications(ws: websocket.WebSocket):
    ws.send_binary(msgpack.packb({
        'action': 'get_notifications',
        'data': {
            'parameter': 'parameter_value'
        }
    }))

    result = msgpack.unpackb(ws.recv(), encoding='utf-8')

    assert result['action'] == 'get_notifications'
    assert result['result'] == True
    assert len(result['data']) == 1
    assert result['data'][0]['title'] == 'Title test'
    assert result['data'][0]['description'] == 'Description test'
    assert result['data'][0]['point']['coordinates'] == [0, 0]


def test_voting(ws: websocket.WebSocket):
    ws.send_binary(msgpack.packb({
        'action': 'get_notifications',
        'data': {
            'parameter': 'parameter_value'
        }
    }))

    result = msgpack.unpackb(ws.recv(), encoding='utf-8')
    id = result['data'][0]['id']

    ws.send_binary(msgpack.packb({
        'action': 'vote_up',
        'data': id
    }))

    result = msgpack.unpackb(ws.recv(), encoding='utf-8')

    if result['action'] == 'vote_up':
        assert result == {
            'action': 'vote_up',
            'result': True
        }

        result = msgpack.unpackb(ws.recv(), encoding='utf-8')

        assert result['action'] == 'updated_notification'
        assert result['data']['rating'] == 1
    elif result['action'] == 'updated_notification':
        assert result['data']['rating'] == 1

        result = msgpack.unpackb(ws.recv(), encoding='utf-8')

        assert result == {
            'action': 'vote_up',
            'result': True
        }
    else:
        pytest.fail('Invalid action')

    ws.send_binary(msgpack.packb({
        'action': 'vote_up',
        'data': id
    }))

    result = msgpack.unpackb(ws.recv(), encoding='utf-8')

    assert result == {
        'action': 'vote_up',
        'result': False,
        'reason': Errors.user_already_voted
    }

    ws.send_binary(msgpack.packb({
        'action': 'vote_down',
        'data': id
    }))

    result = msgpack.unpackb(ws.recv(), encoding='utf-8')

    if result['action'] == 'vote_down':
        assert result == {
            'action': 'vote_down',
            'result': True
        }

        result = msgpack.unpackb(ws.recv(), encoding='utf-8')

        assert result['action'] == 'updated_notification'
        assert result['data']['rating'] == -1
    elif result['action'] == 'updated_notification':
        assert result['data']['rating'] == -1

        result = msgpack.unpackb(ws.recv(), encoding='utf-8')

        assert result == {
            'action': 'vote_down',
            'result': True
        }
    else:
        pytest.fail('Invalid action')
