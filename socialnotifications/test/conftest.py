import os
import pytest
import websocket


@pytest.yield_fixture(scope='session')
def server_address():
    os.environ.setdefault('RTF_SETTINGS', 'socialnotifications.settings')
    from rethinkframework.management.commands_handling import run_test_server

    thread = next(run_test_server())
    yield '127.0.0.1:8001'

    thread.stop()
    thread.join()


@pytest.yield_fixture(scope='session')
def ws(server_address) -> websocket.WebSocket:
    socket = websocket.WebSocket()
    socket.connect('ws://' + server_address)

    yield socket

    socket.close()


