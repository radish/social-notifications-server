import pytest
import msgpack
import websocket

from socialnotifications.status_codes import Errors


def test_valid_register(ws: websocket.WebSocket):
    ws.send_binary(msgpack.packb({
        'action': 'register',
        'data': {
            'email': 'test@test.com',
            'password': 'test',
            'username': 'test',
            'name': 'Test',
            'surname': 'User'
        }
    }))

    result = msgpack.unpackb(ws.recv(), encoding='utf-8')

    assert result == {
        'action': 'register',
        'result': True
    }


def test_register_with_existing_email(ws: websocket.WebSocket):
    ws.send_binary(msgpack.packb({
        'action': 'register',
        'data': {
            'email': 'test@test.com',
            'password': 'test',
            'username': 'test',
            'name': 'Test',
            'surname': 'User'
        }
    }))

    result = msgpack.unpackb(ws.recv(), encoding='utf-8')

    assert result == {
        'action': 'register',
        'result': False,
        'reason': Errors.email_already_registered
    }


def test_login_with_invalid_password(ws: websocket.WebSocket):
    ws.send_binary(msgpack.packb({
        'action': 'login',
        'data': {
            'email': 'test@test.com',
            'password': 'testt'
        }
    }))

    result = msgpack.unpackb(ws.recv(), encoding='utf-8')

    assert result == {
        'action': 'login',
        'result': False,
        'reason': Errors.wrong_password
    }


def test_login_with_invalid_email(ws: websocket.WebSocket):
    ws.send_binary(msgpack.packb({
        'action': 'login',
        'data': {
            'email': 'testt@test.com',
            'password': 'test'
        }
    }))

    result = msgpack.unpackb(ws.recv(), encoding='utf-8')

    assert result == {
        'action': 'login',
        'result': False,
        'reason': Errors.user_does_not_exist
    }


def test_valid_login(ws: websocket.WebSocket):
    ws.send_binary(msgpack.packb({
        'action': 'login',
        'data': {
            'email': 'test@test.com',
            'password': 'test'
        }
    }))

    result = msgpack.unpackb(ws.recv(), encoding='utf-8')

    assert result == {
        'action': 'login',
        'result': True
    }
