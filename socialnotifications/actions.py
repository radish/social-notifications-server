from typing import Dict, Any

from aiohttp.web_ws import WebSocketResponse

from socialnotifications.authentication.controller import AuthenticationController
from socialnotifications.notifications.controller import NotificationsController


class ActionsHandler:
    def __init__(self, socket: WebSocketResponse):
        authentication_controller = AuthenticationController()
        notifications_controller = NotificationsController(authentication_controller, socket)

        self.__actions = {
            'login': authentication_controller.login,
            'register': authentication_controller.register,
            'add_notification': notifications_controller.add_notification,
            'get_notifications': notifications_controller.get_notifications,
            'subscribe': notifications_controller.subscribe,
            'vote_up': notifications_controller.vote_up,
            'vote_down': notifications_controller.vote_down
        }

    async def handle(self, action: str, data: Dict[str, Any]) -> Dict[str, Any]:
        try:
            return await self.__actions[action](data)
        except KeyError:
            pass
