#!/usr/bin/env python
import os
import sys

if __name__ == '__main__':
    os.environ.setdefault('RTF_SETTINGS', 'socialnotifications.settings')

    from rethinkframework.management import execute_command

    execute_command(sys.argv[1:])
